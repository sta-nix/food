import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import 'package:food/Detailpagesample.dart';
import './Component11.dart';
// import './Home2.dart';
import 'package:adobe_xd/specific_rect_clip.dart';
import './Addressset.dart';
import './Component31.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'hscroll.dart';

class Home extends StatelessWidget {
  // home2({
  //   Key key,
  // }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(16.0, 218.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Component11(),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 332.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeInOutExpo,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 490.0),
            child: SpecificRectClip(
              rect: Rect.fromLTWH(0, 0, 796, 94),
              child: UnconstrainedBox(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 795,
                  height: 94,
                  child: GridView.count(
                    primary: false,
                    padding: EdgeInsets.all(0),
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 5,
                    childAspectRatio: 1.5212765957446808,
                    children: [
                      {},
                      {},
                      {},
                      {},
                      {},
                    ].map((map) {
                      return Transform.translate(
                        offset: Offset(-16.0, -490.0),
                        child: Stack(
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(16.0, 490.0),
                              child: Container(
                                width: 143.0,
                                height: 94.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: const Color(0xffffffff),
                                  border: Border.all(
                                      width: 1.0,
                                      color: const Color(0xff707070)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(262.0, 218.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeInOutExpo,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(262.0, 332.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeInOutExpo,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(139.0, 218.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeInOutExpo,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(139.0, 332.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeInOutExpo,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(44.0, 259.0),
            child: SizedBox(
              width: 42.0,
              child: Text(
                'bread',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(7.0, 446.0),
            child: SizedBox(
              width: 116.0,
              child: Text(
                '최근 본 상품',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w800,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 819.0),
            child: Text(
              '야미 매거진',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w800,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(319.0, 819.0),
            child: Text(
              '전체보기',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 12,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(19.0, 373.0),
            child: SizedBox(
              width: 92.0,
              child: Text(
                'Korean cake',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(169.0, 259.0),
            child: SizedBox(
              width: 38.0,
              child: Text(
                'susie',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(146.0, 373.0),
            child: SizedBox(
              width: 84.0,
              child: Text(
                'Side dishes',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(293.0, 259.0),
            child: SizedBox(
              width: 36.0,
              child: Text(
                'Fruit',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(298.0, 373.0),
            child: SizedBox(
              width: 26.0,
              child: Text(
                'etc',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 72.0),
            child: Container(
              width: 375.0,
              height: 126.0,
              decoration: BoxDecoration(
                color: const Color(0xfffec82e),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(134.0, 123.0),
            child: SizedBox(
              width: 108.0,
              child: Text(
                'AD banner',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(137.0, 22.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Addressset(),
                ),
              ],
              child: SizedBox(
                width: 102.0,
                child: Text(
                  '범일동 514',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 20,
                    color: const Color(0xff707070),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 693.0),
            child: Container(
              width: 375.0,
              height: 100.0,
              decoration: BoxDecoration(
                color: const Color(0xfffff3c9),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(134.0, 738.0),
            child: SizedBox(
              width: 108.0,
              child: Text(
                'AD banner',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 872.0),
            child: SpecificRectClip(
              rect: Rect.fromLTWH(0, 0, 670, 190),
              child: UnconstrainedBox(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 660,
                  height: 190,
                  child: GridView.count(
                    primary: false,
                    padding: EdgeInsets.all(0),
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 4,
                    childAspectRatio: 0.7894736842105263,
                    children: [
                      {},
                      {},
                      {},
                      {},
                    ].map((map) {
                      return Transform.translate(
                        offset: Offset(-16.0, -872.0),
                        child: Stack(
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(16.0, 872.0),
                              child: Container(
                                width: 150.0,
                                height: 190.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: const Color(0xffffffff),
                                  border: Border.all(
                                      width: 1.0,
                                      color: const Color(0xff707070)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 597.0),
            child: Component31(),
          ),
          Transform.translate(
            offset: Offset(237.0, 27.0),
            child: SvgPicture.string(
              _shapeSVG_c11819eadef24ae39a4f448a91bf3ec4,
              allowDrawingOutsideViewBox: true,
            ),
          ),
        ],
      ),
    );
  }
}

const String _shapeSVG_c11819eadef24ae39a4f448a91bf3ec4 =
    '<svg viewBox="237.0 27.0 13.0 11.0" ><path transform="translate(237.0, 27.0)" d="M 5.639073371887207 1.456952571868896 C 6.026134967803955 0.8019249439239502 6.973864555358887 0.8019249439239502 7.360926628112793 1.456952571868896 L 12.10847759246826 9.491271018981934 C 12.50238800048828 10.15788650512695 12.02185153961182 11.00000095367432 11.24755191802979 11 L 1.752448320388794 11 C 0.9781482815742493 11.00000095367432 0.4976126253604889 10.15788650512695 0.8915217518806458 9.491271018981934 Z" fill="#fec82e" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
