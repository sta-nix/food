import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './join.dart';
import './home.dart';
import 'h.dart';
import 'hscroll.dart';

class Login extends StatelessWidget {
  Login({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(164.0, 31.0),
            child: SizedBox(
              width: 48.0,
              child: Text(
                'logo',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(22.0, 77.0),
            child: Container(
              width: 335.0,
              height: 46.0,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(22.0, 145.0),
            child: Container(
              width: 335.0,
              height: 46.0,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(100.0, 298.0),
            child: SizedBox(
              width: 180.0,
              child: Text(
                '앗! 비밀번호를 잊어버리셨군요?',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 12,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(128.0, 358.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => join(),
                ),
              ],
              child: SizedBox(
                width: 124.0,
                child: Text(
                  '간편 회원가입 하세요!',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 12,
                    color: const Color(0xff707070),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(121.0, 227.0),
            child: SizedBox(
              width: 134.0,
              child: Text(
                '야미의 시간 로그인',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xffffffff),
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(38.0, 91.0),
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '간편 회원가입 아이디 입력'
              ),
            ),
            // child: Text(
            //   '간편 회원가입 아이디 입력',
            //   style: TextStyle(
            //     fontFamily: 'Apple SD Gothic Neo',
            //     fontSize: 15,
            //     color: const Color(0xff707070),
            //     fontWeight: FontWeight.w300,
            //   ),
            //   textAlign: TextAlign.left,
            // ),
          ),
          Transform.translate(
            offset: Offset(22.0, 213.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home(),
                ),
              ],
              child: Container(
                width: 335.0,
                height: 46.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffc810),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
