import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './home.dart';
import 'h.dart';
import 'hscroll.dart';

class Tutorial3 extends StatelessWidget {
  Tutorial3({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(156.0, 300.0),
            child: SizedBox(
              width: 64.0,
              child: Text(
                'image',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(166.0, 536.0),
            child: Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.elliptical(5.0, 5.0)),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(183.0, 536.0),
            child: Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.elliptical(5.0, 5.0)),
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(183.0, 536.0),
            child: Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.elliptical(5.0, 5.0)),
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(200.0, 536.0),
            child: Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.elliptical(5.0, 5.0)),
                color: const Color(0xfffece47),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 596.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home(),
                ),
              ],
              child: Container(
                width: 335.0,
                height: 46.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffc810),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(114.0, 610.0),
            child: SizedBox(
              width: 148.0,
              child: Text(
                '야미의 시간 시작하기',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xffffffff),
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
