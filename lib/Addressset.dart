import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './home.dart';
import 'h.dart';
import 'hscroll.dart';

class Addressset extends StatelessWidget {
  Addressset({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(57.0, 27.0),
            child: SizedBox(
              width: 86.0,
              child: Text(
                '내 주소 설정',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 72.0),
            child: SizedBox(
              width: 244.0,
              child: Text(
                '지번, 도로명, 건물명을 입력하세요.',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(137.0, 166.0),
            child: SizedBox(
              width: 102.0,
              child: Text(
                '현 위치로 설정',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 101.0),
            child: Container(
              width: 335.0,
              height: 46.0,
              decoration: BoxDecoration(
                color: const Color(0xffe5e5e5),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 114.0),
            child: Text(
              '비밀번호 다시 입력',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff777777),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushRight,
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home(),
                ),
              ],
              child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
