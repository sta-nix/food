import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:adobe_xd/page_link.dart';
import './Detailpagesample.dart';
import 'details.dart';
import 'hscroll.dart';

class Myshoppingbasket extends StatelessWidget {
  Myshoppingbasket({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(119.0, 166.0),
            child: SizedBox(
              width: 138.0,
              child: Text(
                'Coming soon',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(82.0, 22.0),
            child: SizedBox(
              width: 212.0,
              child: Text(
                'My shopping basket',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(170.0, 57.0),
            child: SizedBox(
              width: 36.0,
              child: Text(
                'list',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(25.5, 95.5),
            child: SvgPicture.string(
              _shapeSVG_c79ff47ac8e44c2d96f4f80cf1773201,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 290.0),
            child: Text(
              '예약수량',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 366.0),
            child: Text(
              '총 결제금액',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 328.0),
            child: Text(
              '포장비',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(-1.0, 70.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, 539.0),
                  child: Container(
                    width: 376.0,
                    height: 58.0,
                    decoration: BoxDecoration(
                      color: const Color(0xfffec82e),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(118.0, 560.0),
                  child: SizedBox(
                    width: 148.0,
                    child: Text(
                      '오늘의 야미 결제하기',
                      style: TextStyle(
                        fontFamily: 'Apple SD Gothic Neo',
                        fontSize: 15,
                        color: const Color(0xff707070),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushRight,
                  duration: 0.3,
                  ease: Curves.linear,
                  pageBuilder: () => Home2(),
                ),
              ],
              child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _shapeSVG_c79ff47ac8e44c2d96f4f80cf1773201 =
    '<svg viewBox="25.5 95.5 325.0 176.0" ><path transform="translate(25.5, 95.5)" d="M 0 0 L 325 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(25.5, 271.5)" d="M 0 0 L 325 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
