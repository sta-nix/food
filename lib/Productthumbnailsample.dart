import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './Detailpagesample.dart';
import './Component21.dart';
import './home.dart';
import 'h.dart';
import 'hscroll.dart';

class Productthumbnailsample extends StatelessWidget {
  Productthumbnailsample({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(0.0, 72.0),
                child: PageLink(
                  links: [
                    PageLinkInfo(
                      transition: LinkTransition.PushLeft,
                      duration: 0.3,
                      ease: Curves.easeInOutExpo,
                      pageBuilder: () => Detailpagesample(),
                    ),
                  ],
                  child: Container(
                    width: 375.0,
                    height: 232.0,
                    decoration: BoxDecoration(
                      color: const Color(0xffe8e8e8),
                    ),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(15.0, 317.0),
                child: Text(
                  'Product name 1~2 line',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 20,
                    color: const Color(0xff707070),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Transform.translate(
                offset: Offset(15.0, 348.0),
                child: Text(
                  '16,000',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 14,
                    color: const Color(0xff707070),
                    decoration: TextDecoration.lineThrough,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Transform.translate(
                offset: Offset(15.0, 372.0),
                child: Text(
                  '40% 9,600',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 31,
                    color: const Color(0xffff4e00),
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Transform.translate(
                offset: Offset(250.0, 348.0),
                child: Text(
                  '야미의 시간 23:50',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 14,
                    color: const Color(0xfffd4e1e),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
          Transform.translate(
            offset: Offset(0.0, 361.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, 72.0),
                  child: PageLink(
                    links: [
                      PageLinkInfo(
                        transition: LinkTransition.PushLeft,
                        duration: 0.3,
                        ease: Curves.easeInOutExpo,
                        pageBuilder: () => Detailpagesample(),
                      ),
                    ],
                    child: Container(
                      width: 375.0,
                      height: 232.0,
                      decoration: BoxDecoration(
                        color: const Color(0xffe8e8e8),
                      ),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 317.0),
                  child: Text(
                    'Product name 1~2 line',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 20,
                      color: const Color(0xff707070),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 348.0),
                  child: Text(
                    '16,000',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 14,
                      color: const Color(0xff707070),
                      decoration: TextDecoration.lineThrough,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 372.0),
                  child: Text(
                    '40% 9,600',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 31,
                      color: const Color(0xffff4e00),
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(250.0, 348.0),
                  child: Text(
                    '야미의 시간 23:50',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 14,
                      color: const Color(0xfffd4e1e),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 723.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, 72.0),
                  child: PageLink(
                    links: [
                      PageLinkInfo(
                        transition: LinkTransition.PushLeft,
                        duration: 0.3,
                        ease: Curves.easeInOutExpo,
                        pageBuilder: () => Detailpagesample(),
                      ),
                    ],
                    child: Container(
                      width: 375.0,
                      height: 232.0,
                      decoration: BoxDecoration(
                        color: const Color(0xffe8e8e8),
                      ),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 317.0),
                  child: Text(
                    'Product name 1~2 line',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 20,
                      color: const Color(0xff707070),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 348.0),
                  child: Text(
                    '16,000',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 14,
                      color: const Color(0xff707070),
                      decoration: TextDecoration.lineThrough,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 372.0),
                  child: Text(
                    '40% 9,600',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 31,
                      color: const Color(0xffff4e00),
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(250.0, 348.0),
                  child: Text(
                    '야미의 시간 23:50',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 14,
                      color: const Color(0xfffd4e1e),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 1084.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, 72.0),
                  child: PageLink(
                    links: [
                      PageLinkInfo(
                        transition: LinkTransition.PushLeft,
                        duration: 0.3,
                        ease: Curves.easeInOutExpo,
                        pageBuilder: () => Detailpagesample(),
                      ),
                    ],
                    child: Container(
                      width: 375.0,
                      height: 232.0,
                      decoration: BoxDecoration(
                        color: const Color(0xffe8e8e8),
                      ),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 317.0),
                  child: Text(
                    'Product name 1~2 line',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 20,
                      color: const Color(0xff707070),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 348.0),
                  child: Text(
                    '16,000',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 14,
                      color: const Color(0xff707070),
                      decoration: TextDecoration.lineThrough,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 372.0),
                  child: Text(
                    '40% 9,600',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 31,
                      color: const Color(0xffff4e00),
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(250.0, 348.0),
                  child: Text(
                    '야미의 시간 23:50',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 14,
                      color: const Color(0xfffd4e1e),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(0.0, 597.0),
                child: Container(
                  width: 375.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xfffcfcfc),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(83.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffcf8d),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(83.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(153.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(223.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(293.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(110.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(110.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(180.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(250.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(320.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Stack(
                children: <Widget>[
                  Transform.translate(
                    offset: Offset(13.0, 597.0),
                    child: Container(
                      width: 70.0,
                      height: 70.0,
                      decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(40.0, 612.0),
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                        color: const Color(0xffacacac),
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(42.0, 645.0),
                    child: SizedBox(
                      width: 12.0,
                      child: Text(
                        '홈',
                        style: TextStyle(
                          fontFamily: 'Apple SD Gothic Neo',
                          fontSize: 10,
                          color: const Color(0xff707070),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              Transform.translate(
                offset: Offset(107.0, 645.0),
                child: SizedBox(
                  width: 22.0,
                  child: Text(
                    '검색',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(173.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '내주변',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(243.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '내정보',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(313.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '더보기',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(0.5, 597.5),
                child: Component21(),
              ),
            ],
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 0.3,
                  ease: Curves.easeInOutExpo,
                  pageBuilder: () => Home(),
                ),
              ],
              child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
