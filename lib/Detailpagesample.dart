import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './Myshoppingbasket.dart';
import './Productthumbnailsample.dart';
import './home.dart';
import './Component21.dart';

class Detailpagesample extends StatelessWidget {
  Detailpagesample({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(0.0, 72.0),
            child: Container(
              width: 375.0,
              height: 232.0,
              decoration: BoxDecoration(
                color: const Color(0xffe8e8e8),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 322.0),
            child: Text(
              'Store name',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(137.0, 171.0),
            child: Text(
              'Store image',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 539.0),
            child: Container(
              width: 376.0,
              height: 58.0,
              decoration: BoxDecoration(
                color: const Color(0xfffec82e),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(143.0, 560.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.Fade,
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Myshoppingbasket(),
                ),
              ],
              child: SizedBox(
                width: 98.0,
                child: Text(
                  '장바구니 담기',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 15,
                    color: const Color(0xff707070),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 369.0),
            child: Text(
              '영업시간',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(78.0, 369.0),
            child: Text(
              '10:00 ~ 22:30',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 399.0),
            child: Text(
              '주소',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(78.0, 399.0),
            child: Text(
              '부산시 동구 중앙대로 514 한성기린 프라자',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(131.0, 322.0),
            child: Text(
              'bumildong',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(233.0, 322.0),
            child: Text(
              '0.1km',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushRight,
                  duration: 0.3,
                  ease: Curves.linear,
                  pageBuilder: () => Productthumbnailsample(),
                ),
              ],
              child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(0.0, 597.0),
                child: Container(
                  width: 375.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xfffcfcfc),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(83.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffcf8d),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(83.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(153.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(223.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(293.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(110.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(110.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(180.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(250.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(320.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Stack(
                children: <Widget>[
                  Transform.translate(
                    offset: Offset(13.0, 597.0),
                    child: PageLink(
                      links: [
                        PageLinkInfo(
                          transition: LinkTransition.PushLeft,
                          duration: 0.3,
                          ease: Curves.easeInOutExpo,
                          pageBuilder: () => Home(),
                        ),
                      ],
                      child: Container(
                        width: 70.0,
                        height: 70.0,
                        decoration: BoxDecoration(
                          color: const Color(0xffffffff),
                        ),
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(40.0, 612.0),
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                        color: const Color(0xffacacac),
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(42.0, 645.0),
                    child: SizedBox(
                      width: 12.0,
                      child: Text(
                        '홈',
                        style: TextStyle(
                          fontFamily: 'Apple SD Gothic Neo',
                          fontSize: 10,
                          color: const Color(0xff707070),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              Transform.translate(
                offset: Offset(107.0, 645.0),
                child: SizedBox(
                  width: 22.0,
                  child: Text(
                    '검색',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(173.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '내주변',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(243.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '내정보',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(313.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '더보기',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(0.5, 597.5),
                child: Component21(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
