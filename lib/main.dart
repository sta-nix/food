import 'package:device_preview/device_preview.dart';
import 'package:food/Login.dart';
import 'package:flutter/material.dart';

// void main() => runApp(DevicePreview( builder: (context)=> MyApp(),),);//for pv
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // builder: DevicePreview.appBuilder,// for pv
      title: 'Flutter xD Demo',
      theme: ThemeData(        
        primarySwatch: Colors.blue,
      ),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
      home: Login(),
    );
  }
}
// class Home extends StatelessWidget {
//   const Home({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }