import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import './Component21.dart';

class Component31 extends StatelessWidget {
  Component31({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Transform.translate(
          offset: Offset(0.5, 0.5),
          child: Component21(),
        ),
        Container(
          width: 375.0,
          height: 70.0,
          decoration: BoxDecoration(
            color: const Color(0xfffcfcfc),
          ),
        ),
        Transform.translate(
          offset: Offset(13.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(83.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffcf8d),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(83.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(153.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(223.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(293.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(40.0, 15.0),
          child: Container(
            width: 16.0,
            height: 16.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
              color: const Color(0xffacacac),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(110.0, 15.0),
          child: Container(
            width: 16.0,
            height: 16.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
              color: const Color(0xffacacac),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(110.0, 15.0),
          child: Container(
            width: 16.0,
            height: 16.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
              color: const Color(0xffacacac),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(180.0, 15.0),
          child: Container(
            width: 16.0,
            height: 16.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
              color: const Color(0xffacacac),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(250.0, 15.0),
          child: Container(
            width: 16.0,
            height: 16.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
              color: const Color(0xffacacac),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(320.0, 15.0),
          child: Container(
            width: 16.0,
            height: 16.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
              color: const Color(0xffacacac),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(42.0, 48.0),
          child: SizedBox(
            width: 12.0,
            child: Text(
              '홈',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(107.0, 48.0),
          child: SizedBox(
            width: 22.0,
            child: Text(
              '검색',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(173.0, 48.0),
          child: SizedBox(
            width: 30.0,
            child: Text(
              '내주변',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(243.0, 48.0),
          child: SizedBox(
            width: 30.0,
            child: Text(
              '내정보',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(313.0, 48.0),
          child: SizedBox(
            width: 30.0,
            child: Text(
              '더보기',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}
